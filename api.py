import json
import numpy as np
from flask import Flask, render_template, request, abort, jsonify, Response
from sklearn.externals import joblib
import pandas as pd

app = Flask(__name__)

@app.route('/', methods=['POST'])
def make_prediction():
    
    model = joblib.load('model.pkl')

    data = pd.DataFrame(request.json, index = ['user'])
    
    # categorical = ['month','day','type','marital_status','employment_status']
    # print(data[categorical])
    # data[categorical] = data[categorical]
    # df_dummies = pd.get_dummies(data[categorical])
    # zero =  np.zeros((1,23))

    # print(df_dummies.columns)
    # data = data.drop(data[categorical],axis=1)
    # data = pd.concat([data, df_dummies],axis=1)
    # data = pd.concat([data, zero],axis=0)
    # print(data)
    data = data.to_json(orient='index')
    

    # customers = []
    # customer_id = []
    # data = request.json

    # for item in list(data.keys()):
    #     customer_id.append(item)
    #     item = np.array(list(data[item].values())).reshape(1,-1)
    #     customers.append(item)
    data = json.loads(data)
    #item = np.array(list(data.items())).reshape(1,-1)
    print(data['user'].values())
    item = np.array(list(data['user'].values())).reshape(1,-1)
    # customers = np.vstack(customers)
    print(type(item))
    

    label = model.predict(item)
    print("prediction ok")

    # label = dict(zip(customer_id, prediction))
    # for k,v in label.items():
    #     label[k] = int(v)

    # label = np.random.randint(0,1)
    print(label)
    

    return jsonify({"label":int(label[0])})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=True)